const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

const schema = new Schema({
	paymentStatus		: String,
	paymentLink			: String,
    createdAt       	: { type : Date, default : Date.now },
    updatedAt       	: { type : Date, default : Date.now },
    creator         	: { type : String, default : 'System' },
    updater         	: { type : String, default : 'System' },
    status          	: { type : String, default : 'active' }
});


class Payment {
}

schema.loadClass(Payment);

module.exports = Mongoose.model('Payment', schema);