const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

const schema = new Schema({
	username        : String,
    password        : String,	
    createdAt       : { type : Date, default : Date.now },
    updatedAt       : { type : Date, default : Date.now },
    creator         : { type : String, default : 'System' },
    updater         : { type : String, default : 'System' },
    status          : { type : String, default : 'active' }
});


class User {
}

schema.loadClass(User);

module.exports = Mongoose.model('User', schema);