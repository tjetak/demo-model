const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

const schema = new Schema({
	productCode        : String,
	productName        : String,
    productDescription : String,
    createdAt       : { type : Date, default : Date.now },
    updatedAt       : { type : Date, default : Date.now },
    creator         : { type : String, default : 'System' },
    updater         : { type : String, default : 'System' },
    status          : { type : String, default : 'active' }
});


class Product {
}

schema.loadClass(Product);

module.exports = Mongoose.model('Product', schema);