const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

const schema = new Schema({
	amount				: Number,
	quantity			: String,
	product        		: { type: Schema.Types.ObjectId, ref: 'Product' },
    createdAt      		: { type : Date, default : Date.now },
    updatedAt       	: { type : Date, default : Date.now },
    creator         	: { type : String, default : 'System' },
    updater         	: { type : String, default : 'System' },
    status          	: { type : String, default : 'active' }
});


class Order {
}

schema.loadClass(Order);

module.exports = Mongoose.model('Order', schema);