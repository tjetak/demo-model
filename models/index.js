module.exports = {
	Order: require('./order'),
	Product: require('./product'),
	Invoice: require('./invoice'),
	Payment: require('./payment'),
	User : require('./user'),
};