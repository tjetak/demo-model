const Mongoose = require('mongoose');

const Schema = Mongoose.Schema;

const schema = new Schema({
	invoiceNumber	   	: String,
	orders        		: [{ type: Schema.Types.ObjectId, ref: 'Order' }],
	description        	: String,
	amount				: Number,
	deliveryAddress		: String,
	deliveryName		: String,
    createdAt       	: { type : Date, default : Date.now },
    updatedAt       	: { type : Date, default : Date.now },
    creator         	: { type : String, default : 'System' },
    updater         	: { type : String, default : 'System' },
    status          	: { type : String, default : 'active' }
});


class Invoice {
}

schema.loadClass(Invoice);

module.exports = Mongoose.model('Invoice', schema);