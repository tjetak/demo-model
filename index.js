const Mongoose = require('mongoose');

let model = {
    connect : async (connString) => {
		try {
			await Mongoose.connect(connString, {
				useNewUrlParser: true,
				useUnifiedTopology: true
			});
			console.log('MongoDB connected jos lagi !!');
			return "Connected"
		} catch (err) {
			console.log('Failed to connect to MongoDB', err);
		}
	},
    models : require('./models')
};

module.exports = model;